#!/usr/bin/python
# -*- coding: utf-8 -*-

from scrapy.http import FormRequest, Request
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from product_spider.items import *
from scrapy.exceptions import *
import re

#TODO apple store dates still not parsed.

class ProSpider(CrawlSpider):
    name = "Pro"
    allowed_domains = ["hhgregg.com"]
    start_urls = [
        "http://www.hhgregg.com/"
    ]

    def preprocess_link(link):
        #not sure if I need to unpack javascript or the "href" will just do.
        #print(link)
        return link

    rules = [
        Rule(
            SgmlLinkExtractor(
                restrict_xpaths=("""//div[re:test(@class,"^TopNav.*Column$")]/a[starts-with(@id,"WC_CachedHeaderDisplay_links")]""",),
                process_value=preprocess_link
                ),
            callback="parse_section"),
        ]

    def parse_section(self, response):
        self.log("Visiting section %s at %s" % ("stuff", response.url))

        sel = Selector(response)

        paging_box = sel.xpath("""//div[@class="header_bar"]/span[@class="text"]/span[@class="paging"]""")

        #two variants. Either we have "paging" or "showing"

        is_paging = paging_box.xpath("""./span[@class="paging"]""")
        is_showing = paging_box.xpath("""./div[@class="showing_prod"]""")

        is_single_page = (len(paging_box.xpath("""./span[@class="paging"]/a[@title="Next"]""")) == 0) and \
            (len(paging_box.xpath("""./span[@class="paging"]/a[@title="Back"]""")) == 0)

        if is_single_page:
            self.log("This is single page")
        else:
            #variant 1
            numbers_candidates = is_paging.xpath("""./text()""").extract()

            num_pages = None
            page_index = None

            for n in numbers_candidates:

                match = re.search("(\d+)\s*-\s*(\d+)", n)

                if match != None:
                    groups = match.groups()
                    num_pages = int(groups[1])
                    page_index = int(groups[0])
                    break
            
            if page_index == 1:
                #we add some more pages to parse yet
                header_script = sel.xpath("""//body[@id="sub"]/script/text()""").extract()[0]

                first_addr_ix = header_script.find("http")
                end_of_first_addr = header_script.find("');", first_addr_ix)

                the_address = header_script[first_addr_ix:end_of_first_addr]

                def extract_parameter(pname):
                    param_pos = the_address.find(pname) + len(pname)
                    param_end = the_address.find("&", param_pos)
                    param = the_address[param_pos:param_end]
                    return param

                if num_pages > 1:
                    for i in range(page_index,num_pages): #indexing folloging pages from 1 (initial is 0)!
                        #thank you, firebug!
                        request = FormRequest(the_address,
                            formdata = {
                                "beginIndex" : str(i*12),
                                "catalogId" : extract_parameter("catalogId"),
                                "contentBeginIndex" : "0",
                                "facet" : extract_parameter("facet"),
                                "isHistory" : "false",
                                "langId" : extract_parameter("langId"),
                                "maxPrice" : "",
                                "minPrice" : "",
                                "objectId" : "",
                                "orderBy" : "6",
                                "orderByContent" : "",
                                "pageView" : "image",
                                "productBeginIndex" : str(i*12),
                                "requesttype" : "ajax",
                                "resultType" : "products",
                                "searchTerm" : "",
                                "storeId" : extract_parameter("storeId"),
                                },
                            callback=self.parse_section)
                        self.log("emiting reguest for page %s of %s" %(i+1, num_pages+1))
                        yield request

            else: #page_index > 1
                self.log("Arrived to page %s of %s" % (page_index, num_pages))

            
        
        #no matter what, we do request product sub-pages as well

        links_to_items = sel.xpath("""//a[re:test(@id, "^WC_CatalogEntry.*detailed_link$")]/@href""").extract()

        for link in links_to_items:
            request = Request("http://www.hhgregg.com" + link,
                callback=self.parse_item)

            yield request


    def parse_item(self, response):
        self.log("Visiting item %s" % response.url)

        sel = Selector(response)

        item = ProItem()

        available = len(sel.xpath("""//div[contains(@class, "available_soon_text")]""")) == 0

        def strip_and_join(strlist):
            return "".join([line.strip() for line in strlist if len(line.strip()) > 0])

        if available:
            pricetag = sel.xpath(""".//div[starts-with(@class, "your_price")]/span[contains(@class,"price")]/text()""").extract()[0].strip()
            #currency = Field()          #(string) Currency of the current_price
            if "$" in pricetag:
                item['currency'] = "USD"
            else:
                raise DropItem("Currency other than USD not implemented")

            item['current_price'] = pricetag[1:]

        #original_price = Field()    #(float) Original “MSRP” price of a product. Usually substantially higher than the “current” price
        #not present

        #description = Field()       #(string) Product long description, may contain some specifications, usually has advertising copy
        item['description'] = strip_and_join(sel.css("#Features").xpath(""".//div[@class="features_list"]/preceding-sibling::div[1]/p[1]/text()""").extract())

        #brand = Field()             #(string) Product Brand Name, this is populated by the pipeline, not by the spider
        #OK

        #title = Field()             #(string)Product headline containing the brand and product name
        title_cand = strip_and_join(sel.css(".catalog_link").xpath(".//text()").extract())
        item['title'] = title_cand[:title_cand.find("(Model")]

        model_no = sel.css(".model_no").xpath(".//text()").extract()[0].strip()
        just_model_no = model_no[model_no.find(":")+1:-1].strip()

        #model = Field()             #(string) Product Model Number
        item['model'] = just_model_no
        
        #mpn = Field()               #(string) Manufacturer’s Product Number
        mpn_box = sel.xpath("""//span[contains(text(), "Manufacturer Model Number")]""")
        if len(mpn_box) > 0:
            mpn_val = mpn_box.xpath("""../following-sibling::span[1]/text()""").extract()[0].strip()

        #upc = Field()               #(string) Universal Product Code
        upc_box = sel.xpath("""//span[contains(text(), "UPC Code")]""")
        if len(upc_box) > 0:
            upc_val = upc_box.xpath("""../following-sibling::span[1]/text()""").extract()[0].strip()
            item['upc'] = upc_val


        #primary_image_url = Field() #(string) URL of the main product image
        #image_urls = Field()        #(string list) String Array of additional image urls
        
        product_id = response.url.split("/")[-1]
        
        item['retailer_id'] = product_id      #(string) Retailer Unique Product Identifier (this could be renamed product_id)
        
        #this should be done more bulletproof:
        item['primary_image_url'] = "http://hhgregg.scene7.com/is/image/hhgregg/" + product_id + "_a1_main" 
        #additional urls can be generated similarly, although:
        #1) the base url should be extracted from javascript to support page updates
        #2) the algorithm behind "_a1_main" could be hacked in order to know further suffixes
        
        #features = Field()          #(string list) String array of feature objects usually containing marketing bullet points and verbiage
        
        item['features'] = strip_and_join(sel.css("#Features").xpath(""".//div[@class="features_list"]//text()""").extract())
        
        #specifications = Field()    #(dict) Dict array of specification objects in {name: specname, value: specvalue format}
        #trail = Field()             #(string list) Ordered string array (highest level category first) of categories

        #undone. Looks like some javascript reverse-engineering OR execution prior crawling is necessary.
        #rating = Field()            #(string) Product rating normalized to 100 scale: 3.5/5 = 70
        #rating_box = sel.xpath("""//span[contains(@class, "pr-snippet-rating-decimal")]""")
        #if len(rating_box) > 0:
        #    rating = rating_box.xpath("""./text()""").extract()[0].strip()
            

        #available_instore = Field() #(boolean) Is this product available in store?
        #available_online = Field()  #(boolean) Is this product available for purchase online?
        item['available_online'] = item['available_instore'] = available

        # I don't know where from to get these:
        #sku = Field()               #(string) Product SKU

        yield item
