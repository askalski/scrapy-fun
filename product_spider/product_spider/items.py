#!/usr/bin/python
# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field

class ProItem(Item):
    currency = Field()          #(string) Currency of the current_price
    current_price = Field()     #(float) Current price of the product
    original_price = Field()    #(float) Original “MSRP” price of a product. Usually substantially higher than the “current” price
    description = Field()       #(string) Product long description, may contain some specifications, usually has advertising copy
    brand = Field()             #(string) Product Brand Name, this is populated by the pipeline, not by the spider
    title = Field()             #(string)Product headline containing the brand and product name
    retailer_id = Field()       #(string) Retailer Unique Product Identifier (this could be renamed product_id)
    model = Field()             #(string) Product Model Number
    mpn = Field()               #(string) Manufacturer’s Product Number
    sku = Field()               #(string) Product SKU
    upc = Field()               #(string) Universal Product Code
    image_urls = Field()        #(string list) String Array of additional image urls
    primary_image_url = Field() #(string) URL of the main product image
    features = Field()          #(string list) String array of feature objects usually containing marketing bullet points and verbiage
    specifications = Field()    #(dict) Dict array of specification objects in {name: specname, value: specvalue format}
    trail = Field()             #(string list) Ordered string array (highest level category first) of categories
    rating = Field()            #(string) Product rating normalized to 100 scale: 3.5/5 = 70
    available_instore = Field() #(boolean) Is this product available in store?
    available_online = Field()  #(boolean) Is this product available for purchase online?
    pass
