# Scrapy settings for location_spider project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'location_spider'

SPIDER_MODULES = ['location_spider.spiders']
NEWSPIDER_MODULE = 'location_spider.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'location_spider (+http://www.yourdomain.com)'
