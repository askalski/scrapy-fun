#!/usr/bin/python
# -*- coding: utf-8 -*-

from scrapy.http import FormRequest, Request
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from location_spider.items import *
from scrapy.exceptions import *
import re

#TODO apple store dates still not parsed.

class LocSpider(CrawlSpider):
    name = "Loc"
    allowed_domains = ["apple.com", "wetseal.com"]
    start_urls = [
        "http://www.apple.com/retail/storelist/",
        "http://www.wetseal.com/Stores"
    ]

    rules = [
        Rule(
            SgmlLinkExtractor(
                allow=(r"^http://www.apple.com/(.*)/retail/(.*)$", ),
                restrict_xpaths=("""//div[re:test(@id,"^.*stores$")]/div/ul/li/a""",),
                #process_value=preprocess_apple
                ),
            callback="parse_item_apple"),

        Rule(
            SgmlLinkExtractor(
                #allow=(r"^http://www.apple.com/(.*)/retail/(.*)$", ),
                restrict_xpaths=("""//a[@href="http://www.wetseal.com/Stores"]""",),
                ),
            callback="extract_from_wetseal_storepage"),
        
        ]

    def extract_from_wetseal_storepage(self, response):
        self.log("Visiting main Wetseal Stores page")

        sel = Selector(response)

        form_post_address = sel.xpath("""//form[@id="dwfrm_storelocator_state"]/@action""").extract()[0]
        form_post_options = sel.xpath("""//form[@id="dwfrm_storelocator_state"]/fieldset/div/select/option/@value""").extract()[1:] #first one is ""

        for state in form_post_options:
            request = FormRequest(form_post_address,
                formdata = {
                    "dwfrm_storelocator_address_states_stateUSCA" : state,
                    "dwfrm_storelocator_findbystate" : "Search",
                    },
                callback = self.parse_statepage_wetseal)

            request.meta['tmp_state'] = state

            yield request


    def parse_statepage_wetseal(self, response):
        sel = Selector(response)

        state_name = sel.css(".storelocator-title").extract()[0].split()[-2]
        self.log("Visited Wetseal state %s at %s" % (state_name, response.url))

        shop_boxes = sel.xpath("""//tr[starts-with(@class,"storelocator")]""")

        for shop_box in shop_boxes:

            addr = shop_box.xpath(""".//a[starts-with(@href, "/on/demandware")]/@href""").extract()[0]

            request = Request("http://www.wetseal.com" + addr,
                callback=self.parse_item_wetseal)

            phone = shop_box.xpath(""".//td[@class="store-address"]/text()""").extract()[-1].strip()

            request.meta['tmp_phone_number'] = phone
            request.meta['tmp_state'] = response.meta['tmp_state']

            yield request


    def parse_item_wetseal(self, response):
        self.log("Visited Wetseal store %s" % response.url)

        sel = Selector(response)

        item = LocItem()

        item['store_name'] = sel.xpath("""//div[@class="store-locator-details"]/img/@alt""").extract()[0]
        item['store_url'] = response.url

        two_line_addr = sel.xpath("""//div[@class="store-locator-details"]/p/strong/text()""").extract()

        city_zipcode = two_line_addr[1].split(',')
        item['city'] = city_zipcode[0].strip()
        item['zipcode'] = city_zipcode[1].strip()
        item['address'] = two_line_addr
        item['country'] = "us"

        hours_lines = [line.replace("\n", " ").strip() for line in sel.xpath("""//div[@class="store-locator-details"]/p[2]/text()""").extract()]
        hours_dict = {}
        for line in hours_lines:
            first_colon_ix = line.find(":")
            days = line[:first_colon_ix]

            hours = line[first_colon_ix+1:].split("-")

            hours_dict[days] = {"open" : hours[0].strip(), "close" : hours[1].strip()}

        item['hours'] = hours_dict
        
        item['phone_number'] = response.meta['tmp_phone_number']

        item['state'] = response.meta['tmp_state']

        item['store_id'] = sel.xpath("""//div[@class="store-locator-details"]/h1/text()""").extract()[0].strip()
        
        #not present:
        #store_email = Field()           # (string) Store Email
        #store_floor_plan_url = Field()  # (string) URL of store floor plan map
        #store_image_url = Field()       # (string) Picture of the store
        #weekly_ad_url = Field()         # (url) URL of weekly ad / circular

        return item


    def parse_hours_apple(self, sel, country):
        """
        deliveres map {days_of_week : {open : time, close : time}}
        keys are not necessary days, since to implement range recognition I'd have to translate
        from 10+ languages first.
        """
        days = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"]

        lines = sel.xpath("""//table[1][@class="store-info"]/tr[position()>1]""")

        result = {}

        for line in lines:
            text = line.extract()

            left_pad = line.xpath("""./td[1]/text()""").extract()[0]
            right_pad = line.xpath("""./td[2]/text()""").extract()[0]

            left_pad = left_pad.lower().strip().replace(" ", "").replace(":","")

            if "closed" in right_pad.lower():
                continue

            right_pad = right_pad.split("-")

            result[left_pad] = {"open" : right_pad[0].strip(), "close" : right_pad[0].strip()}

        return result
        
    def parse_item_apple(self, response):
        self.log("Visited Apple store %s" % response.url)

        sel = Selector(response)

        item = LocItem()

        address_and_phone = [line.strip() for line in sel.xpath("""//address[1][@class="store-info"]/*/text()""").extract()]
        item['address'] = address_and_phone[:-1] #change this line if you want phone number to be a part of the address as well.
        #item['phone_number'] = item['address'][-1]

        phone_box = sel.xpath("""//address[1][@class="store-info"]/span[@class="telephone-number"]/text()""")
        if len(phone_box) > 0:
            item['phone_number'] = phone_box.extract()[0]

        try:
            #more reliable way
            item['city'] = sel.xpath("""//address[1][@class="store-info"]/span[@class="locality"]/text()""").extract()[0]
        except IndexError:
            #fallback - sometimes the city name is indistinguishable from state name, address or even alphanumeric zip-code
            #without some amount of artificial intelligence
            city_line = address_and_phone[-2]
            first_digit = re.search("\d", city_line)
            first_digit = first_digit.start() if first_digit != None else len(city_line)
            item['city'] = city_line[:first_digit].strip()

        country_groups = re.match("^http://www.apple.com/(.*)retail/(.*)$", response.url).groups()
        if len(country_groups) < 2:
            country = "us"
        else:
            country = country_groups[0][:-1]

        item['country'] = country
        item['hours'] = self.parse_hours_apple(sel, country)

        region_box = sel.xpath("""//address[1][@class="store-info"]/span[@class="region"]/text()""")
        if len(region_box) > 0:
            item['state'] = region_box.extract()[0]

        #item['store_email'] - unable to find
        item['store_image_url'] = sel.xpath("""//div[@id="gallery-mapSwap"]/div/div/img/@src""").extract()[0]

        #this is controversial - not sure if that's what you wanted
        item['store_id'] = response.url.split("/")[-2]
        
        item['store_name'] = sel.xpath("""//address[1][@class="store-info"]/div[@class="store-name"]/text()""")[0].extract().strip()

        return item
