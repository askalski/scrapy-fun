# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field

class LocItem(Item):
    city = Field() 					# (string) City the store is located in
    address = Field() 				# (string_list) All lines of the address block, one line per list entry
    country = Field() 				# (string)Country name or abbreviation
    hours = Field() 				# (dict) Store hours are stored according to the following format: { day-of-week: { open: time, close: time }, ... }
    phone_number = Field() 			# (string) Store phone number
    services = Field() 				# (string list) Store services
    state = Field() 				# (string) State name or abbreviation
    store_email = Field() 			# (string) Store Email
    store_floor_plan_url = Field() 	# (string) URL of store floor plan map
    store_id = Field() 				# (string) Store number or ID
    store_image_url = Field() 		# (string) Picture of the store
    store_name = Field() 			# (string) Name of the store
    store_url = Field() 			# (string) URL to specific store information page
    weekly_ad_url = Field() 		# (url) URL of weekly ad / circular
    zipcode = Field() 				# (string) 5 digit zip code





"""

"""